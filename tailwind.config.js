/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./*.{html,js}"],
  theme: {
    extend: {
      backgroundImage: {
        'image': "url('../img/foto.jpeg')"
      }
    },
  },
  plugins: [],
}
